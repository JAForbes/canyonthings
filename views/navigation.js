Views.Navigation = Views.View.extend({
  className: 'nav bar',

  initialize: function(){
    this.render();
  },

  render: function(){
    this.$el.html('Bio&nbsp;&nbsp;&nbsp;&nbsp;Games&nbsp;&nbsp;&nbsp;&nbsp;Blog');
  }

});