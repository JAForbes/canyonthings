Views.Main = Views.View.extend({

  views : {},
  className: 'view main',

  initialize: function(){
    this.views = {
      blog : new Views.FoldableColumn({heading:'blog'}),
      bio : new Views.FoldableColumn({heading:'bio'})
    };
    this.render();
    that = this;
    $(window).resize(function(){
      that.arrange.call(that);
    });
  },

  arrange: function(){
    console.log('arrange');
    var totalW = 0;
    _(this.views).each(function(view,viewName){
      view.$el.css({x:totalW});
      totalW += view.$el.width();
    });
  },

  template: function(){
    return _.map(this.views,function(view){
      return view.$el;
    });
  },

  render: function(){
    this.$el.empty();
    $('body').append(this.template());
    this.arrange();
  }
});