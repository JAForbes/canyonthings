Views.FoldableColumn = Views.View.extend({
	className : 'foldable column',

	initialize: function(options){
		this.heading = options && options.heading || 'Heading';
    this.render();
	},

	template: function(){
		return $('<h1>'+this.heading+'</h1>');
	},

	render: function(){
		this.$el.append(this.template());
	}

});