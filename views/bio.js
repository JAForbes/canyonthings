Views.Bio = Views.View.extend({
  className: 'column bio',

  initialize: function(){
    this.render();
  },

  render: function(){
    this.$el.html('This is the bio.  <br>Blah blah blah.');
  }

});